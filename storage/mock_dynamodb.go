package storage

import (
	"../transactions"
	"github.com/stretchr/testify/mock"
)

type mockDynamoDB struct {
	mock.Mock
}

func (mock *mockDynamoDB) getRandomSubset() ([]transactions.PaymentData, error) {
	args := mock.Called()
	resultPayment := args.Get(0)
	resultError := args.Get(1)
	if resultPayment == nil {
		if resultError == nil {
			return nil, nil
		}
		return nil, resultError.(error)
	}
	if resultError == nil {
		return resultPayment.([]transactions.PaymentData), nil
	}
	return nil, nil
}

func (mock *mockDynamoDB) delete(id string) error {
	panic("should not be called")
}

func (mock *mockDynamoDB) putItem(data transactions.PaymentData) error {
	panic("should not be called")
}

func (mock *mockDynamoDB) getItem(id string) (transactions.PaymentData, error) {
	args := mock.Called(id)
	resultPayment := args.Get(0)
	resultError := args.Get(1)
	if resultPayment == nil {
		if resultError == nil {
			return nil, nil
		}
		return nil, resultError.(error)
	}
	if resultError == nil {
		return resultPayment.(transactions.PaymentData), nil
	}
	return nil, nil
}

func (mock *mockDynamoDB) tableNames() ([]*string, error) {
	args := mock.Called()
	return args.Get(0).([]*string), nil
}

func (mock *mockDynamoDB) queueTableCreation(name string) error {
	mock.Called(name)
	return nil
}
