package storage

import (
	"../transactions"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestShouldCreateTable(t *testing.T) {
	logger, _ := test.NewNullLogger()
	dbUrl, _ := url.Parse("http://doesnotmatter.com")

	dynamoMock := new(mockDynamoDB)
	tableName := PaymentsTableName
	dynamoMock.On("queueTableCreation", tableName).Return()
	// no table at first query, the initializer should decide to create a table
	dynamoMock.On("tableNames").Return([]*string{}).Times(1)
	// we need to wait a bit before it appears
	dynamoMock.On("tableNames").Return([]*string{}).Times(3)
	// and finally it's there
	dynamoMock.On("tableNames").Return([]*string{&tableName}).Times(1)

	newMock := func(url *url.URL, logger *logrus.Logger) (DynamoDB, error) {
		return dynamoMock, nil
	}
	uninitializedStorage := NewUninitializedDynamoDBStorage(dbUrl, logger, newMock)

	db, _ := uninitializedStorage.Initialize()

	dynamoMock.AssertExpectations(t)
	assert.Equal(t, db.db, dynamoMock)
}

func TestShouldPassEmptyDataThrough(t *testing.T) {
	dynamoMock := new(mockDynamoDB)
	paymentId := uuid.New()
	dynamoMock.On("getItem", paymentId.String()).Return(nil, nil)
	storage := dynamoDbStorage(dynamoMock)

	returned, _ := storage.Get(paymentId)

	var emptyPayment *transactions.Payment = nil
	assert.Equal(t, emptyPayment, returned)
}

func TestShouldDecodeCollectionOfPayments(t *testing.T) {
	dynamoMock := new(mockDynamoDB)
	firstPaymentId := uuid.New()
	firstPaymentData := transactions.PaymentData{
		IdKeyName:        firstPaymentId.String(),
		"Some other key": "Some other value",
	}
	secondPaymentId := uuid.New()
	secondPaymentData := transactions.PaymentData{
		IdKeyName:         secondPaymentId.String(),
		"Yet another key": "Some another value",
	}
	paymentData := []transactions.PaymentData{firstPaymentData, secondPaymentData}
	dynamoMock.On("getRandomSubset").Return(paymentData, nil)
	storage := dynamoDbStorage(dynamoMock)

	r, _ := storage.GetRandomSubset()
	returned := *r

	assert.Equal(t, 2, len(returned))
}

func TestShouldDecodePaymentData(t *testing.T) {
	dynamoMock := new(mockDynamoDB)
	firstPaymentId := uuid.New()
	firstPaymentData := transactions.PaymentData{
		IdKeyName:  firstPaymentId.String(),
		"Some key": "Some value",
	}
	paymentData := []transactions.PaymentData{firstPaymentData}
	dynamoMock.On("getRandomSubset").Return(paymentData, nil)
	storage := dynamoDbStorage(dynamoMock)

	r, _ := storage.GetRandomSubset()
	returned := *r

	if returned[0].Id != firstPaymentId {
		t.Fail()
	}
	if returned[0].Data["Some key"] != firstPaymentData["Some key"] {
		t.Fail()
	}
}

func dynamoDbStorage(dynamoMock *mockDynamoDB) *DynamoDBStorage {
	tableName := PaymentsTableName
	dynamoMock.On("tableNames").Return([]*string{&tableName}).Times(1)
	logger, _ := test.NewNullLogger()
	dbUrl, _ := url.Parse("http://doesnotmatter.com")
	newMock := func(url *url.URL, logger *logrus.Logger) (DynamoDB, error) {
		return dynamoMock, nil
	}
	uninitializedStorage := NewUninitializedDynamoDBStorage(dbUrl, logger, newMock)
	db, _ := uninitializedStorage.Initialize()
	return db
}
