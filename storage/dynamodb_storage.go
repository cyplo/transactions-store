package storage

import (
	"../transactions"
	"github.com/google/uuid"
)

func (storage DynamoDBStorage) Put(payment transactions.Payment) error {
	storage.logger.Debugf("Storing payment with id: %s", payment.Id.String())
	return storage.db.putItem(payment.Data)
}

func (storage DynamoDBStorage) Get(id uuid.UUID) (*transactions.Payment, error) {
	data, err := storage.db.getItem(id.String())
	if err != nil {
		return nil, err
	}
	if data == nil {
		return nil, nil
	}
	return &transactions.Payment{Id: id, Data: data}, nil
}

func (storage DynamoDBStorage) Delete(id uuid.UUID) error {
	storage.logger.Debugf("Deleting payment with id: %s", id.String())
	return storage.db.delete(id.String())
}

func (storage DynamoDBStorage) GetRandomSubset() (*[]*transactions.Payment, error) {
	paymentsData, err := storage.db.getRandomSubset()

	if err != nil {
		return nil, err
	}

	payments := make([]*transactions.Payment, len(paymentsData))
	for index, paymentData := range paymentsData {
		payments[index], err = paymentFromData(paymentData)
		if err != nil {
			return nil, err
		}
	}

	return &payments, nil
}

func paymentFromData(data transactions.PaymentData) (*transactions.Payment, error) {
	id, err := uuid.Parse(data[IdKeyName].(string))
	if err != nil {
		return nil, err
	}
	return &transactions.Payment{Id: id, Data: data}, nil

}
