package storage

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
	"net/url"
	"time"
)

const InitCheckDuration = 1 * time.Second

type UninitializedDynamoDBStorage struct {
	logger      *logrus.Logger
	url         *url.URL
	constructor dynamoDBConstructor
}

func NewUninitializedDynamoDBStorage(dbUrl *url.URL, logger *logrus.Logger, constructor dynamoDBConstructor) UninitializedDynamoDBStorage {
	return UninitializedDynamoDBStorage{logger: logger, url: dbUrl, constructor: constructor}
}

func (storage UninitializedDynamoDBStorage) Initialize() (*DynamoDBStorage, error) {
	storage.logger.Debugf("Initializing DynamoDB storage (%s)...", storage.url.String())
	db, err := storage.constructor(storage.url, storage.logger)
	if err != nil {
		return nil, err
	}

	var tableNames []*string
	tableNames, err = db.tableNames()

	if err != nil {
		return nil, err
	}

	tableName := PaymentsTableName
	if !funk.Contains(tableNames, &tableName) {
		err = db.queueTableCreation(tableName)

		if err != nil {
			return nil, err
		}

		for !funk.Contains(tableNames, &tableName) {
			time.Sleep(InitCheckDuration)
			tableNames, err = db.tableNames()
			if err != nil {
				return nil, err
			}
		}
	}

	storage.logger.Infof("DynamoDB storage (%s) initialized.", storage.url.String())
	return &DynamoDBStorage{logger: storage.logger, db: db}, nil
}

func NewAWSDynamoDB(url *url.URL, logger *logrus.Logger) (DynamoDB, error) {
	awsLogger := AWSCompliantLogger{logger: logger}

	urlText := url.String()
	sess, err := session.NewSession(&aws.Config{
		Endpoint: &urlText,
	})

	if err != nil {
		return nil, err
	}

	db := dynamodb.New(sess)

	return AWSDynamoDB{logger: logger, awsLogger: awsLogger, db: db}, nil
}
