package storage

import (
	"../transactions"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/sirupsen/logrus"
	"net/url"
)

const PaymentsTableName = "Payments"

type DynamoDBStorage struct {
	logger *logrus.Logger
	db     DynamoDB
}

type dynamoDBConstructor func(url *url.URL, logger *logrus.Logger) (DynamoDB, error)

// this allows for wrapping AWS magic inside
// it only exposes the methods we need; in a way that's easy to use for our usecase
type DynamoDB interface {
	tableNames() ([]*string, error)
	queueTableCreation(string) error
	getItem(id string) (transactions.PaymentData, error)
	getRandomSubset() ([]transactions.PaymentData, error)
	putItem(data transactions.PaymentData) error
	delete(id string) error
}

type AWSDynamoDB struct {
	logger    *logrus.Logger
	awsLogger AWSCompliantLogger
	db        *dynamodb.DynamoDB
}

type AWSCompliantLogger struct {
	logger *logrus.Logger
}

func (logger AWSCompliantLogger) Log(args ...interface{}) {
	logger.logger.Info(args...)
}
