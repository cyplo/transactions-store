package storage

import (
	"../transactions"
	"github.com/google/uuid"
)

const IdKeyName string = "id"

type Storage interface {
	Get(id uuid.UUID) (*transactions.Payment, error)
	Put(payment transactions.Payment) error
	Delete(id uuid.UUID) error
	GetRandomSubset() (*[]*transactions.Payment, error)
}
