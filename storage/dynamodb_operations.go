package storage

import (
	"../transactions"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/davecgh/go-spew/spew"
)

func (d AWSDynamoDB) putItem(data transactions.PaymentData) error {
	d.logger.Debugf("Request to store data in dynamodb: %+#v", data)
	encoded, err := dynamodbattribute.MarshalMap(data)

	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      encoded,
		TableName: aws.String(PaymentsTableName),
	}

	_, err = d.db.PutItem(input)
	if err == nil {
		d.logger.Debug("Stored data")
	}
	return err
}

func (d AWSDynamoDB) getItem(id string) (transactions.PaymentData, error) {
	input := dynamodb.GetItemInput{
		TableName: aws.String(PaymentsTableName),
		Key: map[string]*dynamodb.AttributeValue{
			IdKeyName: {
				S: aws.String(id),
			},
		},
	}

	result, err := d.db.GetItem(&input)

	if err != nil {
		return nil, err
	}

	if len(result.Item) == 0 {
		return nil, nil
	}

	var item transactions.PaymentData
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	return item, err
}

func (d AWSDynamoDB) delete(id string) error {
	input := dynamodb.DeleteItemInput{
		TableName: aws.String(PaymentsTableName),
		Key: map[string]*dynamodb.AttributeValue{
			IdKeyName: {
				S: aws.String(id),
			},
		},
	}

	_, err := d.db.DeleteItem(&input)
	return err
}

func (d AWSDynamoDB) getRandomSubset() ([]transactions.PaymentData, error) {
	input := &dynamodb.ScanInput{
		TableName: aws.String(PaymentsTableName),
	}

	result, err := d.db.Scan(input)

	if err != nil {
		return nil, err
	}

	var items []transactions.PaymentData
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)

	if err != nil {
		return nil, err
	}

	return items, nil
}

func (d AWSDynamoDB) tableNames() ([]*string, error) {
	d.logger.Debug("Asking for a list of tables...")
	tableList, err := d.db.ListTables(&dynamodb.ListTablesInput{})
	if err != nil {
		return nil, err
	}

	d.logger.Debug(spew.Sprintf("Got list of tables: %#v", tableList.TableNames))
	return tableList.TableNames, nil
}

func (d AWSDynamoDB) queueTableCreation(name string) error {
	d.logger.Debugf("Queuing the creation of table '%s'...", name)
	attributes := []*dynamodb.AttributeDefinition{
		{
			AttributeName: aws.String(IdKeyName),
			AttributeType: aws.String("S"),
		},
	}
	keySchema := []*dynamodb.KeySchemaElement{
		{
			AttributeName: aws.String(IdKeyName),
			KeyType:       aws.String("HASH"),
		},
	}
	output, err := d.db.CreateTable(&dynamodb.CreateTableInput{
		TableName:            &name,
		AttributeDefinitions: attributes,
		KeySchema:            keySchema,
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(64),
			WriteCapacityUnits: aws.Int64(64)},
	})
	if err != nil {
		return err
	}
	d.logger.Debugf("Queued with:\n%+v", output)
	return nil
}
