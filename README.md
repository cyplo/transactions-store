# REST payments store service

[![pipeline status](https://gitlab.com/cyplo/transactions-store/badges/master/pipeline.svg)](https://gitlab.com/cyplo/transactions-store/pipelines)


This project is an example of how to implement a REST API to a store(ledger) of bank payments.
It also happens to be my first project in Go - all feedback is welcome !  
[Contact me] directly or leave an issue in this repo :)

Please have in mind that while it tries to mimic a production usecase and have a production-ready design and code quality, it is not deployed to production anywhere.  
If you're trying to design/write a production system like that please feel free to [contact me] !

This is how the system looks like now:

![The actor connects to the app via HTTP using JSON payload, the app stores data in DynamoDB][architecture overview]

While writing this software I needed to make some assumptions about who the client is and what their needs are - please see the [System Design -> Assumptions] document for a full list.

## API

The API is available at `hostname/v1/payment/` - I decided to prefix everything with a version number to allow for an easy transition should the API change drastically in the future.

The endpoints:

- `GET /v1/payment/id` - gets single payment
- `PUT /v1/payment/id` - insert the payment if it does not exist, updates otherwise. The body needs to have at least the `id` field set. All other fields will be stored as they are. The `id` in the URL needs to match the `id` in the data.
- `GET /v1/payment/` - get a list of payments - will get a full list of all payments if the number of them is relatively small, otherwise it will get a random subset of all payments. There is no search/querying functionality yet.
- `DELETE /v1/payment/id` - removes the payment

where `id` is any valid `UUID`.

You can find examples on how to call the API in `test/basic-scenario.json` - it's a [Postman] request collection.

## How to run locally

You can clone the repo and run docker-compose to stand up the stack.

```
git clone https://gitlab.com/cyplo/transactions-store.git
cd transactions-store
./scripts/launch-docker.sh
```

This should boot up the application and any dependencies in a set of docker containers.
It will make the application available under `http://localhost:8000` on the host machine.


### Requirements
- bash
- docker
- docker-compose

# More docs

Make sure to check [contributing], [system design] and [project diary] for a more wholesome experience !

# Future

Please find a list of interesting research directions/future improvements here:

- [ ] Provide deployment automation - kubernetes and/or AWS Lambda. Could be cool to start by using Gitlab's kubernetes infrastructure and grow from there maybe ?
- [ ] Would be interesting to measure performance of the whole stack as it is right now and compare with a lambda deployment of the same code and kubernetes deployment.
- [ ] Functionality of getting a list of payments is not particularly useful as it is right now as it gets a random subset of the all of the payments stored. It would be better to have a querying mechanism instead. Could be interesting to compare REST query API to GraphQL

## Technical TODO list
- [ ] see if we can stop assuming id must be uuid
- [ ] speed up docker builds in local development environment - right now the main test script builds the whole app from scratch, including downloading the dependencies. There is a room for improvement there - maybe share the dependencies cache from the host to the docker container. CI would still be built from scratch, but local dev runs would be faster.
- [ ] make the app handle urls that have a trailing `/` as well as those that don't

[system design]: docs/SYSTEM_DESIGN.md
[system design -> assumptions]: docs/SYSTEM_DESIGN.md
[project diary]: docs/PROJECT_DIARY.md
[contributing]: CONTRIBUTING.md
[sample data]: docs/sample-data.json
[architecture overview]: ./docs/architecture-overview.jpg
[postman]: https://www.getpostman.com/
[contact me]: mailto:hello@cyplo.net
