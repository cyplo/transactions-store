#!/usr/bin/env bash

set -e
set -o pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$HERE/.."

docker-compose up --build -d api
docker-compose up --abort-on-container-exit --exit-code-from scenario-runner scenario-runner
docker-compose up --abort-on-container-exit --exit-code-from spec-runner spec-runner
