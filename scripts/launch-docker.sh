#!/usr/bin/env bash

set -e
set -o pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$HERE/.."

docker-compose up --build api
