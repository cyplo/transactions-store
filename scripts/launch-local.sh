#!/usr/bin/env bash

set -e
set -o pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$HERE/.."

./scripts/build.sh

docker-compose down
docker-compose up -d db
sleep 2
API_PORT=3000 \
DATABASE_URL="http://localhost:8000" \
LOG_LEVEL="debug" \
AWS_REGION="local" \
AWS_ACCESS_KEY_ID="test" \
AWS_SECRET_ACCESS_KEY="test" \
./bin/api
