#!/usr/bin/env bash

set -e
set -o pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$HERE/.."

go get -d -t
! (go fmt ./... 2>&1 | read)
go vet ./...
go build -o bin/api
go test ./...
