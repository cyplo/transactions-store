package payments

import (
	"../../storage"
	"../../transactions"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"net/http"
)

func (h Handler) UpsertSingle(w http.ResponseWriter, r *http.Request) {
	paymentIdFromPath, parseErr := h.paymentIdFromUrlPath(w, r)
	if parseErr != nil {
		return
	}

	h.logger.Infof("Got a request to upsert payment with payment id: %s", paymentIdFromPath)

	payment, decodingError := h.paymentFromRequestBody(w, r)

	if decodingError != nil {
		return
	}

	if *paymentIdFromPath != payment.Id {
		h.logger.Errorf("Payment id in the url path (%s) is not same as payment id in the request body (%s)",
			paymentIdFromPath, payment.Id.String())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	logrus.Debugf("Settled on payment ID: %+v", payment.Id)

	h.storePayment(payment, w)
}

func (h Handler) storePayment(payment *transactions.Payment, w http.ResponseWriter) {
	err := (*h.storage).Put(*payment)
	if err != nil {
		h.logger.WithError(err).Error("Storage error")
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (h Handler) paymentFromRequestBody(w http.ResponseWriter, r *http.Request) (*transactions.Payment, error) {
	var payment transactions.Payment
	decodingError := json.NewDecoder(r.Body).Decode(&payment.Data)

	if decodingError != nil {
		h.logger.Errorf("Cannot parse request body as JSON")
		w.WriteHeader(http.StatusBadRequest)
		return nil, decodingError
	}

	paymentDataIdText := payment.Data[storage.IdKeyName]

	paymentDataId, err := uuid.Parse(paymentDataIdText.(string))
	if err != nil {
		message := "Cannot parse payment id as UUID"
		h.logger.Errorf(message)
		w.WriteHeader(http.StatusBadRequest)
		return nil, errors.New(message)
	}

	payment.Id = paymentDataId

	return &payment, nil
}
