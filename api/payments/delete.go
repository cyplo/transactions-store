package payments

import "net/http"

func (h Handler) Delete(w http.ResponseWriter, r *http.Request) {
	paymentIdFromPath, parseErr := h.paymentIdFromUrlPath(w, r)
	if parseErr != nil {
		return
	}

	h.logger.Infof("Got a request to delete payment with payment id: %s", paymentIdFromPath)

	err := (*h.storage).Delete(*paymentIdFromPath)

	if err != nil {
		h.logger.Errorf("Error deleting payment: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
