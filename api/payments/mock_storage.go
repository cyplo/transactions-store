package payments

import (
	"../../transactions"
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
)

type MockStorage struct {
	mock.Mock
}

func (storage *MockStorage) Put(payment transactions.Payment) error {
	args := storage.Called(payment)
	err := args.Get(0)
	if err == nil {
		return nil
	}
	return err.(error)
}

func (storage *MockStorage) Get(id uuid.UUID) (*transactions.Payment, error) {
	args := storage.Called(id)
	result := args.Get(0)
	err := args.Get(1)
	if err != nil {
		return nil, err.(error)
	}
	if result == nil {
		return nil, nil
	}
	return result.(*transactions.Payment), nil
}

func (storage *MockStorage) Delete(id uuid.UUID) error {
	args := storage.Called(id)
	err := args.Get(0)
	if err == nil {
		return nil
	}
	return err.(error)
}

func (storage *MockStorage) GetRandomSubset() (*[]*transactions.Payment, error) {
	args := storage.Called()
	result := args.Get(0)
	err := args.Get(1)
	if err != nil {
		return nil, err.(error)
	}
	if result == nil {
		return nil, nil
	}
	return result.(*[]*transactions.Payment), nil
}
