package payments

import (
	"../../storage"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

type Handler struct {
	paymentIdUrlVariableName string
	storage                  *storage.Storage
	logger                   *logrus.Logger
}

const UrlPath = "/v1/payment"

const VariableName = "id"

func HandlerWith(storage storage.Storage, logger *logrus.Logger) http.Handler {
	handler := Handler{paymentIdUrlVariableName: VariableName, storage: &storage, logger: logger}
	return newRouter(handler)
}

func newRouter(handler Handler) *mux.Router {
	parent := mux.NewRouter()
	router := parent.PathPrefix(UrlPath).Subrouter()

	router.Use(addContentType)
	router.HandleFunc("/{"+VariableName+"}", handler.GetSingle).Methods("GET")
	router.HandleFunc("/", handler.GetRandomSubset).Methods("GET")
	router.HandleFunc("/{"+VariableName+"}", handler.UpsertSingle).Methods("PUT")
	router.HandleFunc("/{"+VariableName+"}", handler.Delete).Methods("DELETE")

	return router
}

func addContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func (h Handler) paymentIdTextFromUrlPath(r *http.Request) string {
	return mux.Vars(r)[h.paymentIdUrlVariableName]
}

func (h Handler) paymentIdFromUrlPath(w http.ResponseWriter, r *http.Request) (*uuid.UUID, error) {
	paymentIdText := h.paymentIdTextFromUrlPath(r)
	paymentIdFromPath, parseErr := uuid.Parse(paymentIdText)
	if parseErr != nil {
		h.logger.Errorf("Cannot parse payment id: %s", paymentIdText)
		w.WriteHeader(http.StatusBadRequest)
		return nil, parseErr
	}

	return &paymentIdFromPath, nil
}
