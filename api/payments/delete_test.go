package payments

import (
	"github.com/gavv/httpexpect"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestDelete(t *testing.T) {
	storage := new(MockStorage)
	paymentId := uuid.New()
	storage.On("Delete", paymentId).Return(nil)
	server, e, logHook := newHandler(t, storage)
	defer server.Close()

	expectDelete(e, paymentId.String()).
		Status(http.StatusOK)

	storage.AssertExpectations(t)

	assert.Equal(t, 1, len(logHook.Entries))
	assert.Contains(t, logHook.LastEntry().Message, paymentId.String())
}

func TestDeleteStorageError(t *testing.T) {
	storage := new(MockStorage)
	paymentId := uuid.New()
	storage.On("Delete", paymentId).Return(errors.New("storage error"))
	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectDelete(e, paymentId.String()).
		Status(http.StatusInternalServerError)
}

func TestDeleteInvalidPaymentId(t *testing.T) {
	invalidPaymentId := "not an uuid"

	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	expectDelete(e, invalidPaymentId).
		Status(http.StatusBadRequest)

	assert.Equal(t, len(logHook.Entries), 1)
	assert.Contains(t, logHook.LastEntry().Message, invalidPaymentId)
}

func expectDelete(e *httpexpect.Expect, paymentId string) *httpexpect.Response {
	return e.DELETE(pathForId(paymentId)).
		Expect().
		ContentType("application/json")
}
