package payments

import (
	"../../storage"
	"../../transactions"
	"encoding/json"
	"fmt"
	"github.com/gavv/httpexpect"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus/hooks/test"
	"net/http/httptest"
	"testing"
)

func newHandler(t *testing.T, s storage.Storage) (*httptest.Server, *httpexpect.Expect, *test.Hook) {
	logger, logHook := test.NewNullLogger()
	handler := HandlerWith(s, logger)
	server := httptest.NewServer(handler)
	e := httpexpect.New(t, server.URL)
	return server, e, logHook
}

func pathForId(id string) string {
	return fmt.Sprintf("%s/%s", UrlPath, id)
}

func NewRandomPayment() transactions.Payment {
	var data transactions.PaymentData
	paymentId := uuid.New()
	dataJson := fmt.Sprintf(`{ "type": "Payment", "id": "%s"}`, paymentId.String())
	json.Unmarshal([]byte(dataJson), &data)
	payment := transactions.Payment{Id: paymentId, Data: data}
	return payment
}
