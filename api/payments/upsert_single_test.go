package payments

import (
	"../../storage"
	"../../transactions"
	"errors"
	"github.com/gavv/httpexpect"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestUpsertStorePayment(t *testing.T) {
	storage := new(MockStorage)
	payment := NewRandomPayment()
	storage.On("Put", payment).Return(nil)
	server, e, logHook := newHandler(t, storage)
	defer server.Close()

	expectUpsert(e, payment).
		Status(http.StatusOK)

	storage.AssertExpectations(t)

	assert.Equal(t, 1, len(logHook.Entries))
	assert.Contains(t, logHook.LastEntry().Message, payment.Id.String())
}

func TestUpsertStorageError(t *testing.T) {
	storage := new(MockStorage)
	payment := NewRandomPayment()
	storage.On("Put", payment).Return(errors.New("storage error"))
	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectUpsert(e, payment).
		Status(http.StatusInternalServerError)
}

func TestUpsertInvalidIdInPath(t *testing.T) {
	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	invalidPaymentId := "not an uuid"
	e.PUT(pathForId(invalidPaymentId)).
		Expect().
		Status(http.StatusBadRequest)

	assert.Equal(t, len(logHook.Entries), 1)
	assert.Contains(t, logHook.LastEntry().Message, invalidPaymentId)
}

func TestUpsertNonJSONBody(t *testing.T) {
	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	paymentId := uuid.New()
	invalidJson := "not a json"
	e.PUT(pathForId(paymentId.String())).
		WithText(invalidJson).
		Expect().
		Status(http.StatusBadRequest)

	assert.Contains(t, logHook.LastEntry().Message, "Cannot parse request body as JSON")
}

func TestUpsertNonUUIDId(t *testing.T) {
	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	paymentId := uuid.New()
	data := transactions.PaymentData{
		storage.IdKeyName: "not a uuid",
		"type":            "Payment",
	}
	e.PUT(pathForId(paymentId.String())).
		WithJSON(data).
		Expect().
		Status(http.StatusBadRequest)

	assert.Contains(t, logHook.LastEntry().Message, "Cannot parse payment id as UUID")
}

func TestUpsertDifferentIDsInPathAndBody(t *testing.T) {
	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	payment := NewRandomPayment()
	differentPaymentId := uuid.New()
	e.PUT(pathForId(differentPaymentId.String())).
		WithJSON(payment.Data).
		Expect().
		Status(http.StatusBadRequest)

	assert.Contains(t, logHook.LastEntry().Message, payment.Id.String())
	assert.Contains(t, logHook.LastEntry().Message, differentPaymentId.String())
}

func expectUpsert(e *httpexpect.Expect, payment transactions.Payment) *httpexpect.Response {
	return e.PUT(pathForId(payment.Id.String())).
		WithJSON(payment.Data).
		Expect().
		ContentType("application/json")
}
