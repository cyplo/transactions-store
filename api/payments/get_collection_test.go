package payments

import (
	"../../transactions"
	"github.com/gavv/httpexpect"
	"github.com/pkg/errors"
	"github.com/thoas/go-funk"
	"net/http"
	"testing"
)

func TestGetRandomSubsetReturnStoredPayments(t *testing.T) {
	firstPayment := NewRandomPayment()
	secondPayment := NewRandomPayment()
	payments := []*transactions.Payment{&firstPayment, &secondPayment}
	paymentsData := funk.Map(payments, func(payment *transactions.Payment) transactions.PaymentData { return payment.Data })

	storage := new(MockStorage)
	storage.On("GetRandomSubset").Return(&payments, nil)

	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectGetAll(e).
		Status(http.StatusOK).
		JSON().Array().
		Equal(paymentsData)

	storage.AssertExpectations(t)
}

func TestGetRandomSubsetStorageError(t *testing.T) {
	storage := new(MockStorage)
	storage.On("GetRandomSubset").Return(nil, errors.New("storage error"))

	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectGetAll(e).Status(http.StatusInternalServerError)
}

func expectGetAll(e *httpexpect.Expect) *httpexpect.Response {
	return e.GET(UrlPath + "/").
		Expect().
		ContentType("application/json")
}
