package payments

import (
	"../../transactions"
	"encoding/json"
	"github.com/thoas/go-funk"
	"net/http"
)

func (h Handler) GetRandomSubset(w http.ResponseWriter, r *http.Request) {

	payments, err := (*h.storage).GetRandomSubset()

	if err != nil {
		h.logger.Errorf("Error getting a list of payments: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	paymentsData := funk.Map(*payments, func(payment *transactions.Payment) transactions.PaymentData {
		return payment.Data
	})

	err = json.NewEncoder(w).Encode(paymentsData)

	if err != nil {
		h.logger.Errorf("Error encoding a list of payments to JSON: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
