package payments

import (
	"errors"
	"github.com/gavv/httpexpect"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestGetReturnStoredPayment(t *testing.T) {
	payment := NewRandomPayment()

	storage := new(MockStorage)
	storage.On("Get", payment.Id).Return(&payment, nil)

	server, e, logHook := newHandler(t, storage)
	defer server.Close()

	expectGet(e, payment.Id.String()).JSON().Object().
		Equal(payment.Data).
		Path("$.id").Equal(payment.Id)

	assert.Equal(t, len(logHook.Entries), 1)
	assert.Contains(t, logHook.LastEntry().Message, payment.Id.String())
}

func TestGetUnknownPaymentId(t *testing.T) {
	storage := new(MockStorage)
	paymentId := uuid.New()
	storage.On("Get", paymentId).Return(nil, nil)

	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectGet(e, paymentId.String()).
		Status(http.StatusNotFound)
}

func TestGetInvalidPaymentId(t *testing.T) {
	invalidPaymentId := "not an uuid"

	server, e, logHook := newHandler(t, new(MockStorage))
	defer server.Close()

	expectGet(e, invalidPaymentId).
		Status(http.StatusBadRequest)

	assert.Equal(t, len(logHook.Entries), 1)
	assert.Contains(t, logHook.LastEntry().Message, invalidPaymentId)
}

func TestGetStorageReadError(t *testing.T) {
	storage := new(MockStorage)
	paymentId := uuid.New()
	storage.On("Get", paymentId).Return(nil, errors.New("storage error"))

	server, e, _ := newHandler(t, storage)
	defer server.Close()

	expectGet(e, paymentId.String()).
		Status(http.StatusInternalServerError)
}

func expectGet(e *httpexpect.Expect, paymentId string) *httpexpect.Response {
	return e.GET(pathForId(paymentId)).
		Expect().
		ContentType("application/json")
}
