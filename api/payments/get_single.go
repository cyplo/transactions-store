package payments

import (
	"../../transactions"
	"encoding/json"
	"github.com/google/uuid"
	"net/http"
)

func (h Handler) GetSingle(w http.ResponseWriter, r *http.Request) {
	paymentId, parseErr := h.paymentIdFromUrlPath(w, r)
	if parseErr != nil {
		return
	}

	h.logger.Infof("Got get request with payment id: %s", paymentId)

	payment, err := h.paymentForId(*paymentId, w, r)

	h.logger.Debugf("Got payment from the storage: %+v", payment)

	if payment == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	h.encodePayment(err, w, payment)
}

func (h Handler) encodePayment(err error, w http.ResponseWriter, payment *transactions.Payment) {
	err = json.NewEncoder(w).Encode(payment.Data)
	if err != nil { // not sure when this would happen tbh
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (h Handler) paymentForId(paymentId uuid.UUID, w http.ResponseWriter, r *http.Request) (*transactions.Payment, error) {
	h.logger.Debug("Getting payment from the storage")
	payment, err := (*h.storage).Get(paymentId)

	if err != nil {
		h.logger.WithError(err).Error("Storage error")
		w.WriteHeader(http.StatusInternalServerError)
		return nil, err
	}

	return payment, nil
}
