package logger

import (
	"github.com/sirupsen/logrus"
	"os"
)

func NewLogger() *logrus.Logger {
	logger := logrus.New()
	configureLogLevel(logger)
	return logger
}

func configureLogLevel(logger *logrus.Logger) {
	levelText, ok := os.LookupEnv("LOG_LEVEL")

	defaultLogLevel := logrus.InfoLevel
	if !ok {
		levelText = defaultLogLevel.String()
	}

	level, parseErr := logrus.ParseLevel(levelText)

	if parseErr != nil {
		level = defaultLogLevel
	}

	logger.SetLevel(level)

	logger.Debugf("Log level set to %s", logger.Level)

	if parseErr != nil {
		logger.Errorf("Error parsing LOG_LEVEL('%s')", levelText)
	}
}
