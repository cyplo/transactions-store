package logger

import (
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestLogLevelValid(t *testing.T) {
	logger, _ := test.NewNullLogger()
	os.Setenv("LOG_LEVEL", "debug")
	configureLogLevel(logger)

	assert.Equal(t, logger.Level, logrus.DebugLevel)
}

func TestLogLevelInvalid(t *testing.T) {
	logger, _ := test.NewNullLogger()
	os.Setenv("LOG_LEVEL", "not a known level")
	configureLogLevel(logger)

	assert.Equal(t, logger.Level, logrus.InfoLevel)
}

func TestLogLevelUnset(t *testing.T) {
	logger, _ := test.NewNullLogger()
	os.Unsetenv("LOG_LEVEL")
	configureLogLevel(logger)

	assert.Equal(t, logger.Level, logrus.InfoLevel)
}
