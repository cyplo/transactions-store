package main

import (
	"./api/payments"
	"./logger"
	"./storage"
	"fmt"
	"net/http"
	"net/url"
	"os"
)

func main() {
	l := logger.NewLogger()
	dbUrlText := os.Getenv("DATABASE_URL")
	dbUrl, err := url.Parse(dbUrlText)

	if err != nil {
		l.Fatalf("Cannot parse db url: %s", dbUrlText)
	}

	uninitializedStorage := storage.NewUninitializedDynamoDBStorage(dbUrl, l, storage.NewAWSDynamoDB)
	dynamoDBStorage, err := uninitializedStorage.Initialize()
	if err != nil {
		l.Fatalf("Error initializing database connection %+v", err)
	}

	router := payments.HandlerWith(dynamoDBStorage, l)
	port := os.Getenv("API_PORT")
	l.Infof("Starting payments api on port %s", port)
	l.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
}
