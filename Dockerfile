FROM golang:alpine AS build
RUN apk add git bash gcc musl-dev --no-cache

ADD . /src/

# separate step to allow fo caching dependencies
RUN cd /src && go get -d -t
RUN cd /src && ./scripts/build.sh

FROM alpine
WORKDIR /app
COPY --from=build /src/bin/api /app/
ENTRYPOINT ./api
