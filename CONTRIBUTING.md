## Contributing

If you'd like to help this project by contributing- thank you !  
To develop locally you need just a few more things than needed for just running the app.

### Requirements

- bash
- docker
- docker-compose
- go toolchain - tested on version `1.11.5`
- newman - to run the scenario tests

### Development

This project was written by writing the scenario tests first and then drilling down into unit tests.  
There is currently one scenario, encoded as postman/newman http request collection with integrated tests.  
The scenario lives under `test/basic-scenario.json`. You can run it by starting the app locally and then using newman or postman:  

- `./scripts/launch-local.sh`
- `newman run test/basic-scenario.json --global-var "API_HOSTNAME=localhost" --global-var "API_PORT=3000"` - or you can use postman to load the collection up from that file and launch requests manually. As you can see the run is parametrized via the environment variables, you can choose to run this scenario against any application host.
 
In addition to that you can use `go test ./...` from the root to run all unit tests - none of the tests written in Go require any external resources to run; they do not connect to a database nor use any local files.

There is `./scripts/test.sh` script, that launches all of the above, every unit and scenario test, to help with development, as well.

### Project directory structure:

#### Non-source code parts:

```
scripts/:
  - build.sh - build and launch local go tests
  - launch-local.sh - launch the database in a docker container and launch the app on the host  
  - launch-docker.sh` - launches the database and the app using docker compose 
  - test.sh` - launch the integration tests suite inside docker containers
docs/ - documentation
test/ - whole-system blackbox tests' data. Postman/newman scenario tests live here.
```

#### Source code layout:
```
- api
  - payments - all the http api handler and routing logic lives here
    - payments_handler.go - the common core of the handler, e.g. routing lives here
    - delete.go - handle delete payment request
    - get_single.go - handle returning a single payment
    - get_collection.go - handle returning multiple payments
    - upsert_single.go - handle create and update requests
- storage - all storage concerns, both generic as well as DynamoDB-specific ones live here.
- transactions - home to the transaction types, right now it's just `Payment` there
- logger - logging utilities
```

Almost all of the directories above have `*_test.go` files with unit tests describing the relevant parts of the code there - if you have a question around how the API really behaves with regards to the corner cases - please feel free to read those - I hope they are clear enough to facilitate just that.

As a side note - I tried making the project structure more centred around `actions` but failed to find a clean layout that would represent that. As an example of what I mean - I would probably like to have "everything that concerns a get request for a payment` in the same directory, http handler responsible for it but also a database read path code. If you know how to do that cleanly - please let me know !

See [system design] for more in-depth guide to the architecture and decisions behind it.
You may find [project diary] interesting as well to get to know how I implemented the design.

[system design]: docs/SYSTEM_DESIGN.md
[project diary]: docs/PROJECT_DIARY.md
