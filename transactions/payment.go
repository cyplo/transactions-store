package transactions

import "github.com/google/uuid"

type PaymentData map[string]interface{}

type Payment struct {
	Id   uuid.UUID
	Data PaymentData
}
