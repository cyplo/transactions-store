# System design

## Current architecture:

Given the requirements and the assumptions I made I decided to use the following:

- Architecture: map the REST http requests to db transactions pretty much 1-to-1, surface any errors as they are - this should be relatively easy to implement, just a thin layer translating between database and the REST API. Will allow for showcaing Go usage, while not being excessive.
- Testing: We need something that can express and run a coarse scenario-based tests on the HTTP level. I chose Postman as a tool to write the scenarios in - the scenarios can include code to express the assertions in and can be run from the commandline.
- Service packaging: static go binary wrapped in docker compose. 
   - Provide docker compose for the ease of development.
   - It's easy to migrate from this model to e.g. deployment to AWS lambdas or kubernetes if needed.
- Deployment infrastructure: provide a way to get a Docker image with the application but do not provide any explicit way to deploy that - a simpler solution, that does not forbid us from going to kubernetes or lambdas in the future.
- Persistence: DynamoDB
    - Docker image to run locally is provided and works well
    - Should be easy to run in the cloud, e.g. AWS DocumentDB
- Developer services (code hosting, CI/CD): gitlab
    - Different services well integrated with each other - handy when experimenting, integrated CI/CD
    - Cost: offers private repos and CI/CD for free 
 
This setup should be fairly easy to get going but also does not prevent us from changing it in the future. I wanted to show a reasonably production-ready environment, but with a limited number of things that are unfamiliar to me, as the project is written in Go, which is a new thing for me already.  

To get to know what options I considered and what are the requirements and assumptions I've made - please read on !

## Requirements

- Expose REST interface
- Some form of data persistence
- Written in Go

## Assumptions

- Looking into the test data, there is a `type` field there, it's always `Payment` though. I imagine there is a 'parent' type of `payment`, something akin to `transaction` maybe. For the sake of simplicity this system will only support `payment` though, and `payment` is a top-level concept in the system, exposed directly in the REST API.
- We do not need to detect any invalid (e.g. duplicated etc) payments, this system's main responsibility is storage.
- While we have access to the [sample data] this system does not necessarily need to parse/understand the data it is storing. It needs, however to be able to identify a single payment (I assume by its id) and be able to list payments. We shouldn't overconstrain the stored data type.
- We do not need to support complex queries, just whatever is needed for the simple REST API. In case of complex queries we could expose the storage under e.g. GraphQL - not needed right now though.
- Any issues with processing one payment should not affect processing of other payments directly
- System should be reasonably easy to change given change in requirements.
- The service should be easy to make it deployed, but not necessarily deployable out of the box. It's okay to provide a set of Docker container definitions with a deployment infrastructure description, but not necessarily solve the problems of authentication, authorization, TLS termination, load balancing etc. The design should not go against any of the well known practices though, the service should be easy to embed into any deployment infrastructure.
- No need to disaster recovery and backup strategies
- Single point of service is enough - no need to georeplicate
- The project should be easy to spin up in the cloud environment but also easy to pick up for a developer.
- The read/write ratio is close to 1:1 for this system, there is no dominant transaction type.
- No other processes will be accessing the database without going through our API - I'm assuming that the data written to the db has been written by the application and henceforth it is valid and consistent 

## Discussion

### Introduction

What we know is that the API should be a REST one and support all 4 of CRUD operations.  

The internals of the system should be easy to change, if the requirements change. This could mean having to deal with changing the external facing API but also changes in the infrastructure type we want to run or the load we need to be able to sustain. In short, the system should not have additional functionality beyond what we know is required right now but should be well tested and easy to change should we need to.  

### Choices

There are multiple ways of designing a system like that, let's list some and see which ones we like !
The items on the list are not necessarily comparable 1-to-1 with each other, they represent and address different facets of the architecture that we can pick and choose from.

#### API surface
- async vs sync processing of requests - in case where processing of our transactions would be of considerable latency - we could make the write requests be async. The initial request would return a ticket/handle/id using which the client could then ask for the status of the transaction. This can be implemented by having some API gateway in front of a queue and then have multiple workers taking work from said queue. As we do not foresee the latency to be a problem now - let's park it for a while.

#### Internal service architecture:

- CQRS, event store - we could separate the read and write paths through the system (CQRS principle) and then use different types of storage for the read and write operations. We could capture events happening in the application and write them to the event store for ease of distributing the system and disaster recovery needs. However, as we do not know the client system that much we probably want to settle for something simpler for now.
- eventual consistency, distributed gossip -> If we would like the system to handle huge load and huge spikes in the load as well as well as to be able to handle very geographically distributed audience we could implement a system where each transaction gets assigned a node that would handle it and then that node would have its own knowledge of the world and would answer based on that, without contacting any central service/database. The node needs to periodically talk to other nodes to update its state of the world, we could use gossip protocols for that, could be e.g. RAFT-based. As we do not expect such requirements for now - let's settle for something simpler.

#### Persistence

- rdbms (e.g. Postgres) - rdbms provide a way of storing JSON documents, some even understand the documents and allow you to query based on document keys (I know Postgres does a good job there), but it's not their natural first use case. RDBMSes shine if we need to maintain referential integrity of data, this does not seem to be a requirement here though. 
- document database - these like to operate on documents, the data we have is a JSON document already. Some of them even can expose a REST API to the data directly, so we could not write any code for this system, if we wouldn't want to write the Go wrapper.
- file/object storage (e.g. S3) - we could just write the json files to some object storage. This has a benefit of simplicity, but can end up complicated if we want to start understanding the data. Easy to sync between environments, easy to backup

#### Deployment infrastructure
- docker + kubernetes - if we want to have full control around the deployment and runtime conditions of the application, we could use kubernetes cluster for that. We could run the persistence engine as well as the applicaiton there and manage how they connect to each other.
- functions/lambdas + hosted persistence (e.g. cloud hosted db or object storage) - less infrastructure to maintain, easy to scale, easy to start with while providing reasonable production experience; note: we probably need to dockerize the services anyway with this scenario, to be able to run the full stack on the development machine.

[sample data]: ./sample-data.json
