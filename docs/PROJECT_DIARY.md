## Project diary

I started at the very outside, drilled in, did some discovery and more tests inside, added read and create primitives and then circled back to the top to add more comprehensive scenario level tests.

* Started with doing some research and writing the design document, to run through the possible search space.
* Wrote the OpenAPI spec and a runner for it - using `dredd`
* Used the spec to drive implementing the HTTP boilerplate (bind to a port, route to relevant functions, set headers) - no separate tests here as I did not want to focus on this and I predict to be fairly static part of the project. If this becomes a hot path, or a feature of its own - it then would probably be tested in place and extracted out. Postponed that for now though.
* Started writing tests in go, loose scenarios without testing particulars - this is to flush out the architecture - separate the storage concern from http concern
* The scenarios lead me to scaffold types (Payment, Storage)
* Around this time, when I had the types and tests scaffolded, I started running them on CI on every push. Most of the CI runs are red now, as I have the high-level tests failing and guiding me on what is missing
* Didn't know how to marry the handler struct with the router, so focused on implementing the first method (get handler by id) knowing that I have a the spec test to remind me to get back to the routing later.
* Figured out how to structure the code more or less now.
* Having GET working on the http handler level, with injected fake storage, I circled back to the outside loop of TDD, and figured out that while the OpenAPI spec is helpful, I would like to have scenario-based tests there as well - introduced some.
* Added postman collection scenario tests, this exposed lack of storage setup in `main()` 
* Seeing that the output of tests is a bit cryptic - test drove adding the logging facilities
If there would be any processing logic - we would probably need to introduce a new type/layer of a test - the one that would test the logic directly instead of going through the http layer.
* tried having separate handler types, one per q request type, but ended up with a lot of duplicated code. I could have injected the duplicated code into several smaller handler types but chose to merge them into one handler for now.
* general rinse and repeat of the above - a lot of trial and error around shaping go code, as this is my first go project.

## I was surprised by:
As this is my first project in Go, I thought it may be educational to write what I was surprised by in the Go ecosystem;  

* the compilation process and the test runner are super fast, this is really nice for TDD 
* variable names shadow type/package names
* there seems to be no map/filter/reduce functions on the collections, in the standard library
* there is no `quick return if there was an error` syntax sugar, that would allow me to skip writing `if err != nil{}` blocks, something akin to Rust's `?` syntax
* you can return partially initialized structures
* there is nil and there are nil pointers
* tools need a special commandline argument to be invoked recursively (fmt, vet, test etc)

Overall I found that it was quite easy for me to grasp enough of the concepts in Go to get me started relatively quickly, the learning curve is nice and gentle.
